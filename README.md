# ChiSense-12

ChiSense-12 is an English sense-annotated child-directed speech corpus. Please refer to the paper "ChiSense-12: An English Sense-Annotated Child-Directed Speech Corpus" (Cabiddu et al., 2022) for detailed information about the dataset.

## Getting started

This project contains the folder *ChiSense-12* including the entire tagged corpus, and the folder *coding_small_study* which contains R code to analyze the results of the small inter-annotator agreement study included in the main paper.

In the folder *ChiSense-12*, each ambiguous word tagged has its own Excel dedicated file. In each file, the variables included are:
* id: CHILDES database utterance number. This identifier can be used to retrieve specific corpus variables including speakers and target children’s information. Please refer to MacWhinney (2000) and Braginsky et al. (2019) for more information about the CHILDES database.
* gloss: target utterance spoken
* speaker_code: the speaker code as in the CHILDES database
* speaker_role: the speaker role in the conversation (e.g., Mother)
* ambiguous_word: ambiguous word considered in the utterance
* ambiguous_meaning: ambiguous word sense tagged
* ambiguous_verb_final_noun: verb stem that takes a tagged ambiguous word as grammatical object

## Annotation process

Information about the annotation process can be found in the main paper. Importantly, when a target utterance could not be tagged based on the sentence context, the surrounding conversational context in CHILDES was considered. In *main.R* (folder *small_coding_study*), an example can be found on how to access the conversational context in the CHILDES database. When an ambiguous word meaning could not be disambiguated based on sentence or surrounding context, the files report a value of NA.  

## References

Braginsky, M., Sanchez, A., & Yurovsky, D. (2019). *childesr: Accessing the ‘CHILDES’ Database*. R package version 0.2.1.

MacWhinney, B. (2000). *The CHILDES Project: Tools for analyzing talk*. Transcription format and programs (Vol. 1). Psychology Press.

## Corresponding author

Francesco Cabiddu, Cardiff University, CabidduF@cardiff.ac.uk

